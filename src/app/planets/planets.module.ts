import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwoComponent } from './two/two.component';
import { PlanetsRoutingModule } from './planets-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PlanetsRoutingModule
  ],
  declarations: [TwoComponent]
})
export class PlanetsModule { }
