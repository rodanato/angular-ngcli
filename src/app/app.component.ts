import { Component } from '@angular/core';

@Component({
  selector: 'rod-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'rod works!';
}
