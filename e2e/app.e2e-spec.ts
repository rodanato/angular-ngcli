import { AngularNgcliPage } from './app.po';

describe('angular-ngcli App', () => {
  let page: AngularNgcliPage;

  beforeEach(() => {
    page = new AngularNgcliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('rod works!');
  });
});
