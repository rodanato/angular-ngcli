import { browser, element, by } from 'protractor';

export class AngularNgcliPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('rod-root h1')).getText();
  }
}
